﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class TableUsuarioFrm : Form
    {

        private DataSet DsUsuarios;
        private BindingSource BsUsuarios;

        public DataSet Dsusuarios { set { DsUsuarios = value; } }
        public TableUsuarioFrm()
        {
            InitializeComponent();
            BsUsuarios = new BindingSource();
        }

        private void btNuevo_Click(object sender, EventArgs e)
        {
            GestionUsuarioFrm guf = new GestionUsuarioFrm();
            guf.TblUsuarios = DsUsuarios.Tables["Usuario"];
            guf.DsUsuarios = DsUsuarios;
            guf.ShowDialog();
        }

        private void TableUsuarioFrm_Load(object sender, EventArgs e)
        {
            BsUsuarios.DataSource = DsUsuarios;
            BsUsuarios.DataMember = DsUsuarios.Tables["Usuario"].TableName;
            dataGridView1.DataSource = BsUsuarios;
            dataGridView1.AutoGenerateColumns = true;


        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BsUsuarios.Filter = string.Format("Nombre like '*{0}*' or Telefono like '*{0}*' or Correo like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
