﻿namespace WindowsFormsApplication1
{
    partial class GestionUsuarioFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtContraseñaConfirm = new System.Windows.Forms.MaskedTextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtContraseña = new System.Windows.Forms.MaskedTextBox();
            this.txtCorreoConfirm = new System.Windows.Forms.TextBox();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.txtTelConfirm = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.lblCoincidencia1 = new System.Windows.Forms.Label();
            this.lblCoincide2 = new System.Windows.Forms.Label();
            this.lblCoincide3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Contraseña";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Confirmar Contraseña";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(73, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Correo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Confirmar Correo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(73, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Telefono";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Confirmar Telefono";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(137, 21);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(380, 20);
            this.txtNombre.TabIndex = 9;
            // 
            // txtContraseñaConfirm
            // 
            this.txtContraseñaConfirm.Location = new System.Drawing.Point(137, 96);
            this.txtContraseñaConfirm.Name = "txtContraseñaConfirm";
            this.txtContraseñaConfirm.PasswordChar = '*';
            this.txtContraseñaConfirm.Size = new System.Drawing.Size(380, 20);
            this.txtContraseñaConfirm.TabIndex = 10;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(137, 131);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(380, 20);
            this.txtCorreo.TabIndex = 11;
            // 
            // txtContraseña
            // 
            this.txtContraseña.Location = new System.Drawing.Point(137, 57);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.PasswordChar = '*';
            this.txtContraseña.Size = new System.Drawing.Size(380, 20);
            this.txtContraseña.TabIndex = 12;
            // 
            // txtCorreoConfirm
            // 
            this.txtCorreoConfirm.Location = new System.Drawing.Point(137, 171);
            this.txtCorreoConfirm.Name = "txtCorreoConfirm";
            this.txtCorreoConfirm.Size = new System.Drawing.Size(380, 20);
            this.txtCorreoConfirm.TabIndex = 13;
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(137, 211);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(380, 20);
            this.txtTel.TabIndex = 14;
            // 
            // txtTelConfirm
            // 
            this.txtTelConfirm.Location = new System.Drawing.Point(137, 253);
            this.txtTelConfirm.Name = "txtTelConfirm";
            this.txtTelConfirm.Size = new System.Drawing.Size(380, 20);
            this.txtTelConfirm.TabIndex = 15;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnSalir);
            this.flowLayoutPanel1.Controls.Add(this.btnGuardar);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 290);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(602, 32);
            this.flowLayoutPanel1.TabIndex = 16;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(524, 3);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(443, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lblCoincidencia1
            // 
            this.lblCoincidencia1.AutoSize = true;
            this.lblCoincidencia1.ForeColor = System.Drawing.Color.Red;
            this.lblCoincidencia1.Location = new System.Drawing.Point(533, 99);
            this.lblCoincidencia1.Name = "lblCoincidencia1";
            this.lblCoincidencia1.Size = new System.Drawing.Size(68, 13);
            this.lblCoincidencia1.TabIndex = 17;
            this.lblCoincidencia1.Text = "No coincide*";
            this.lblCoincidencia1.Visible = false;
            // 
            // lblCoincide2
            // 
            this.lblCoincide2.AutoSize = true;
            this.lblCoincide2.ForeColor = System.Drawing.Color.Red;
            this.lblCoincide2.Location = new System.Drawing.Point(532, 178);
            this.lblCoincide2.Name = "lblCoincide2";
            this.lblCoincide2.Size = new System.Drawing.Size(69, 13);
            this.lblCoincide2.TabIndex = 18;
            this.lblCoincide2.Text = "No Coincide*";
            this.lblCoincide2.Visible = false;
            this.lblCoincide2.Click += new System.EventHandler(this.lblCoincide2_Click);
            // 
            // lblCoincide3
            // 
            this.lblCoincide3.AutoSize = true;
            this.lblCoincide3.ForeColor = System.Drawing.Color.Red;
            this.lblCoincide3.Location = new System.Drawing.Point(533, 257);
            this.lblCoincide3.Name = "lblCoincide3";
            this.lblCoincide3.Size = new System.Drawing.Size(69, 13);
            this.lblCoincide3.TabIndex = 19;
            this.lblCoincide3.Text = "No Coincide*";
            this.lblCoincide3.Visible = false;
            // 
            // GestionUsuarioFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 334);
            this.Controls.Add(this.lblCoincide3);
            this.Controls.Add(this.lblCoincide2);
            this.Controls.Add(this.lblCoincidencia1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.txtTelConfirm);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.txtCorreoConfirm);
            this.Controls.Add(this.txtContraseña);
            this.Controls.Add(this.txtCorreo);
            this.Controls.Add(this.txtContraseñaConfirm);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "GestionUsuarioFrm";
            this.Text = "GestionUsuarioFrm";
            this.Load += new System.EventHandler(this.GestionUsuarioFrm_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.MaskedTextBox txtContraseñaConfirm;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.MaskedTextBox txtContraseña;
        private System.Windows.Forms.TextBox txtCorreoConfirm;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.TextBox txtTelConfirm;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label lblCoincidencia1;
        private System.Windows.Forms.Label lblCoincide2;
        private System.Windows.Forms.Label lblCoincide3;
    }
}