﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Entities
{
    class Usuario
    {
        int id,CantidadAF;
        String Nombre, Contraseña, ContraseñaConfirm, Correo, CorreoConfirm, Tel, TelConfirm;

        public Usuario(int id, int cantidadAF, string nombre, string contraseña, string contraseñaConfirm, string correo, string correoConfirm, string tel, string telConfirm)
        {
            this.Id = id;
            CantidadAF1 = cantidadAF;
            Nombre1 = nombre;
            Contraseña1 = contraseña;
            ContraseñaConfirm1 = contraseñaConfirm;
            Correo1 = correo;
            CorreoConfirm1 = correoConfirm;
            Tel1 = tel;
            TelConfirm1 = telConfirm;
        }
        public Usuario() { }

        public int CantidadAF1
        {
            get
            {
                return CantidadAF;
            }

            set
            {
                CantidadAF = value;
            }
        }

        public string Contraseña1
        {
            get
            {
                return Contraseña;
            }

            set
            {
                Contraseña = value;
            }
        }

        public string ContraseñaConfirm1
        {
            get
            {
                return ContraseñaConfirm;
            }

            set
            {
                ContraseñaConfirm = value;
            }
        }

        public string Correo1
        {
            get
            {
                return Correo;
            }

            set
            {
                Correo = value;
            }
        }

        public string CorreoConfirm1
        {
            get
            {
                return CorreoConfirm;
            }

            set
            {
                CorreoConfirm = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre1
        {
            get
            {
                return Nombre;
            }

            set
            {
                Nombre = value;
            }
        }

        public string Tel1
        {
            get
            {
                return Tel;
            }

            set
            {
                Tel = value;
            }
        }

        public string TelConfirm1
        {
            get
            {
                return TelConfirm;
            }

            set
            {
                TelConfirm = value;
            }
        }
    }
}
