﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class GestionUsuarioFrm : Form
    {

        private DataSet dsUsuarios;
        private DataTable tblUsuarios;
        private BindingSource bsUsuario;

        public DataSet DsUsuarios{ set{ dsUsuarios = value;}}

        public DataTable TblUsuarios{set{tblUsuarios = value;}}

        public GestionUsuarioFrm()
        {
            InitializeComponent();
            bsUsuario = new BindingSource();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void lblCoincide2_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            String n,c,cc,co,coco,t,tc;
            int id;
            n = txtNombre.Text;
            c = txtContraseña.Text;
            cc = txtContraseñaConfirm.Text;
            co = txtCorreo.Text;
            coco = txtCorreoConfirm.Text;
            t = txtTel.Text;
            tc = txtTelConfirm.Text;
            if(c != cc)
            {
                lblCoincidencia1.Show();
            }else if(co != coco)
            {
                lblCoincide2.Show();
            }else if (t != tc)
            {
                lblCoincide3.Show();
            }
            if (c == cc & t==tc& co==coco ) {
                tblUsuarios.Rows.Add(tblUsuarios.Rows.Count + 1, n, c, co, t);
            }
        }

        private void GestionUsuarioFrm_Load(object sender, EventArgs e)
        {
            bsUsuario.DataSource = dsUsuarios;
            bsUsuario.DataMember = dsUsuarios.Tables["Usuario"].TableName;
        }
    }
}
